public class Board{
	private Square[][] tictactoeBoard;
	
	//constructor
	public Board(){
		this.tictactoeBoard = new Square[][]{
			{Square.BLANK,Square.BLANK,Square.BLANK},
			{Square.BLANK,Square.BLANK,Square.BLANK},
			{Square.BLANK,Square.BLANK,Square.BLANK}
		};
	}
	
	public String toString(){
		String boardResult = "";
		for (int i = 0; i < this.tictactoeBoard.length; i++){
			for (int j = 0; j < this.tictactoeBoard[i].length; j++){
				boardResult += this.tictactoeBoard[i][j];
			}
			boardResult += "\n";
		}
		return boardResult;
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if ((row <= 3 && row >0) || (col <=3 && col >0)){
			if (this.tictactoeBoard[row-1][col-1] == Square.BLANK){
				this.tictactoeBoard[row-1][col-1] = playerToken;
				return true;
			}
		} 
		return false;

	}
	
	public boolean checkIfFull(){
		for (int i = 0; i<this.tictactoeBoard.length; i ++){
			for (int j =0; j < this.tictactoeBoard[i].length; j++){
				if (this.tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for (int i = 0; i<this.tictactoeBoard.length; i++){
			int j = 0;
			if (this.tictactoeBoard[i][j] == playerToken && this.tictactoeBoard[i][j + 1] == playerToken && this.tictactoeBoard[i][j + 2] == playerToken){
					return true;
				}
			}
		return false;
		}
		
	private boolean checkIfWinningVertical(Square playerToken){
			int i = 0;
			for (int j = 0; j <tictactoeBoard[i].length; j++){	
				if (this.tictactoeBoard[i][j] == playerToken && this.tictactoeBoard[i+1][j] == playerToken && this.tictactoeBoard[i+2][j] == playerToken){
						return true;
					}
				}
		
		return false;
		}
		
	private boolean checkIfWinningDiagonal(Square playerToken){
		if (this.tictactoeBoard[1][1] == playerToken){
			if ((this.tictactoeBoard[0][0] == playerToken && this.tictactoeBoard[2][2] == playerToken) || (this.tictactoeBoard[0][2] == playerToken && this.tictactoeBoard[2][0] == playerToken)){
				return true;
			}
		}
		return false;
	}	
	
	public boolean checkIfWinning(Square playerToken){
		if (this.checkIfWinningHorizontal(playerToken) == true || this.checkIfWinningVertical(playerToken) == true || this.checkIfWinningDiagonal(playerToken) == true){
			return true;
		}
		return false;
	}
	
	
}
