import java.util.Scanner;

public class TTT{
	public static void main(String[] args){
		//main game values
		boolean gameOver = false;
		int player1Score = 0;
		int player2Score = 0;		
		//Scanner
		Scanner keyboard = new Scanner(System.in);

		while (gameOver != true){
			//initialize game variables
			System.out.println("Welcome to tic tac toe!");
			Board mainBoard = new Board();
			int player = 1;
			Square playerToken = Square.X;
			
			boolean playAgain = true;
			while (playAgain == true){
			System.out.println(mainBoard);
			//player's turn
			if (player == 1){
				playerToken = Square.X;
			}
			else{
				playerToken = Square.O;
			}
			
			//choosing row
			int rowChosen = 0;
			do {
			System.out.println("Player " + player + " please enter row");
			rowChosen = keyboard.nextInt();
			}
			while (rowChosen > 3 || rowChosen <= 0);
			//choosing column
			int columnChosen = 0;
			do {
			System.out.println("Player " + player + " please enter column");
			columnChosen = keyboard.nextInt();
			}
			while (columnChosen > 3 || columnChosen <=0);
			
			
			//checking if spot chosen is empty
			boolean isValid = mainBoard.placeToken(rowChosen, columnChosen, playerToken);
			while (isValid == false){
				System.out.println("please enter a spot that is empty");
			//choosing row
			rowChosen = 0;
			do {
			System.out.println("Player " + player + " please enter row");
			rowChosen = keyboard.nextInt();
			}
			while (rowChosen > 3 || rowChosen <= 0);
			//choosing column
			columnChosen = 0;
			do {
			System.out.println("Player " + player + " please enter column");
			columnChosen = keyboard.nextInt();
			}
			while (columnChosen > 3 || columnChosen <=0);
			isValid = mainBoard.placeToken(rowChosen, columnChosen, playerToken);
			}

			
			//check if game over
			
			
			boolean boardIsFull = mainBoard.checkIfFull();
			//check for tie
			String userAnswer ="";
			if (boardIsFull == true){
				System.out.println("it's a tie!");
				//restarting the game or not
				System.out.println("Do you want to play again??? (yes or no)");
				keyboard.nextLine();
				userAnswer = keyboard.nextLine();
				if (userAnswer.equals("no")){
					gameOver = true;
					playAgain = false;
					System.out.println("score player 1: " + player1Score);
					System.out.println("score player 2: " + player2Score);
				}
				else if (userAnswer.equals("yes")){
					playAgain = false;
				}
			}
			boolean isSomeoneWinning = mainBoard.checkIfWinning(playerToken);
			//check for official winner
			if (isSomeoneWinning == true){
				System.out.println("the winner is player " + player);
				
				//adding points to players
				if (player == 1){
					player1Score++;
				}
				else{
					player2Score++;
				}				
				//restarting the game or not
				System.out.println("Do you want to play again??? (yes or no)");
				keyboard.nextLine();
				userAnswer = keyboard.nextLine();
				if (userAnswer.equals("no")){
					gameOver = true;
					playAgain = false;
					System.out.println("score player 1: " + player1Score);
					System.out.println("score player 2: " + player2Score);
				}
				else if (userAnswer.equals("yes")){
					playAgain = false;
				}
				


			}
			else{
				player +=1;
				if (player > 2){
					player = 1;
				}
			}
		}
		}
	}	
}