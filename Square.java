public enum Square{
	X,
	O,
	BLANK;
	
	public String toString(){
		if (this == BLANK){
			return "_";
		}
		
		else if (this == X){
			return "X";
			
		}
		
		else if (this == O){
			return "O";
		}
		return "?";
	}
}